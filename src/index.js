import createOscillator from "./modules/oscillator.js";
import notes, { frequencies } from "./modules/piano.js";
import createPanner from "./modules/pan.js";
import { initBuffers } from "./modules/reverb.js";
import ReverbNode from "./modules/reverb.js";

const keyboardKeys = [
  "z",
  "s",
  "x",
  "d",
  "c",
  "v",
  "g",
  "b",
  "h",
  "n",
  "j",
  "m",
  "q",
  "2",
  "w",
  "3",
  "e",
  "r",
  "5",
  "t",
  "6",
  "y",
  "7",
  "u",
  "i",
  "9",
  "o",
  "0",
  "p",
];
/**
 * @type {Map<string, number>}
 */
const keyMap = new Map(
  notes.slice(27, 56).map((note, i) => {
    return [keyboardKeys[i], note];
  })
);
const touchFrequencies = frequencies.slice(4, 66);

/**
 * @type {AudioContext}
 */
const context = new (window.AudioContext || window.webkitAudioContext)();
const masterCompressor = context.createDynamicsCompressor();
const masterGain = context.createGain({ gain: 0.8 });
const masterPan = createPanner(context);
const masterAnalyser = context.createAnalyser();
masterAnalyser.fftSize = Math.pow(2, 11);
const analyserSettings = new Map([["analyser", masterAnalyser]]);
[masterPan, masterCompressor, masterAnalyser, context.destination].reduce((graph, node) => graph.connect(node));
initBuffers(context);

/**
 * @typedef {import("./components/OscillatorSettings").OscillatorSettings} OscillatorSettings
 */
/**
 * @type {OscillatorSettings[]}
 */
const oscillatorSettingsEls = Array.from(document.querySelectorAll("oscillator-settings"));
/**
 * @type Map<string, ExtendedOscillator[]>
 */
const playing = new Map();

/**
 * @typedef { import("./modules/oscillator").Note } Note
 */
/**
 *
 * @typedef { import("./modules/oscillator").ExtendedOscillator} ExtendedOscillator
 */

/**
 *
 * @param {Note} note
 * @returns {ExtendedOscillator}
 */
const playNote = (note, touchId) => {
  const source = createOscillator(context, note, touchId);
  source.connect(masterGain);
  return source;
};

const onKeyDown = (e) => {
  const { frequency, names } = keyMap.get(e.key) || {};
  if (frequency && !playing.get(names[0])) {
    playing.set(
      names[0],
      oscillatorSettingsEls.map((oscillatorSettings) => {
        const { type, detune } = oscillatorSettings;
        return playNote({ frequency, type, detune });
      })
    );
  }
};

const onKeyUp = (e) => {
  const { frequency, names } = keyMap.get(e.key) || {};
  if (frequency) {
    const oscillators = playing.get(names[0]);
    if (!Array.isArray(oscillators)) return;
    oscillators.forEach((source) => source.stop());
    playing.delete(names[0]);
  }
};

const handleDetuneChange = (e) => {
  const detune = e.detail;
  for (const sources of playing.values()) {
    for (const source of sources) {
      source.detune.setValueAtTime(detune, context.currentTime);
    }
  }
};

const onWheel = (e) =>
  oscillatorSettingsEls.forEach((oscillatorSettings) => oscillatorSettings.detuneEl.move(e.deltaY));

const playTouchNote = (touchEvent) => {
  playing.set(
    touchEvent.identifier,
    oscillatorSettingsEls.map((oscillatorSettings) =>
      playNote({
        ...getOscillatorSettingsFromEvent(touchEvent),
        type: oscillatorSettings.type,
      }, touchEvent.identifier)
    )
  );
};

const onTouchStart = (e) => {
  Array.from(e.changedTouches).forEach(playTouchNote);
  analyserSettings.set("lineColor", getHslFromTouchlist(e.touches));
};

const oscillatorClamps = {
  frequency: (value) => Math.min(touchFrequencies[touchFrequencies.length - 1], Math.max(touchFrequencies[0], value)),
  detune: (value) => Math.min(1200, Math.max(-1200, value)),
};

const moveOscillator = (oscillator, settings) => {
  Object.entries(oscillatorClamps).forEach(([param, clamp]) => {
    const value = settings[param];
    if (typeof value !== "undefined" && oscillator[param]) {
      oscillator[param].linearRampToValueAtTime(clamp(value), context.currentTime + 0.05);
    }
  });
};

const onTouchMove = (e) => {
  for (const touchEvent of e.changedTouches) {
    const touchOscillators = playing.get(touchEvent.identifier) || [];
    touchOscillators.forEach((oscillator) => {
      moveOscillator(oscillator, getOscillatorSettingsFromEvent(touchEvent));
    });
  }
  analyserSettings.set("lineColor", getHslFromTouchlist(e.touches));
};

const stopChangedTouchNotes = (e) => {
  for (const touch of e.changedTouches) {
    playing.get(touch.identifier).forEach(osc => osc.stop());
    playing.delete(touch.identifier);
  }
  analyserSettings.set("lineColor", getHslFromTouchlist(e.touches));
};

const getHslFromTouchlist = (touchlist) => {
  const touches = Array.from(touchlist);
  if (touches.length === 0) return "";
  const [hue, saturation, lightness] = touches.reduce(
    ([h, s, l], touch, i) => {
      const l2 = touch.clientY / document.body.clientHeight / 3 + 0.54;
      const halfWidth = document.body.clientWidth / 2;
      const s2 = Math.abs(1 - Math.abs(touch.clientX - halfWidth) / halfWidth) / 3 + 0.34;
      const h2 = Math.round(getOscillatorSettingsFromEvent(touch).frequency % 360);
      if (i === touches.length - 1) {
        return [(h + h2) / touches.length, (s + s2) / touches.length, (l + l2) / touches.length];
      }
      return [h + h2, s + s2, l + l2];
    },
    [0, 0, 0]
  );
  return `hsl(${hue}, ${Math.round(saturation * 100)}%, ${Math.round(lightness * 100)}%)`;
};

const getOscillatorSettingsFromEvent = (event) => {
  const frequency =
    touchFrequencies[Math.round((1 - event.clientY / document.body.clientHeight) * touchFrequencies.length)];
  const detune = 0;
  const pan = Math.max(-1, Math.min(1, (2 * event.clientX) / document.body.clientWidth - 1));
  return { frequency, detune, pan };
};

const onMouseMove = (e) => {
  const touchOscillators = playing.get("mouse") || [];
  touchOscillators.forEach((oscillator) => {
    moveOscillator(oscillator, getOscillatorSettingsFromEvent(e));
  });
  analyserSettings.set("lineColor", getHslFromTouchlist([e]));
};

const onMouseDown = (e) => {
  e.identifier = "mouse";
  playTouchNote(e);
  e.target.addEventListener("mousemove", onMouseMove);
  analyserSettings.set("lineColor", getHslFromTouchlist([e]));
};

const onMouseUp = (e) => {
  e.target.removeEventListener("mousemove", onMouseMove);
  analyserSettings.set("lineColor", getHslFromTouchlist([e]));
  const touchOscillators = playing.get("mouse") || [];
  touchOscillators.forEach((oscillator) => {
    oscillator.stop();
  });
};

window.onload = () => {
  const waveformVisualizer = document.querySelector("waveform-visualizer");
  waveformVisualizer.drawAnalyser(analyserSettings);
  oscillatorSettingsEls.forEach((oscillatorSettings) => {
    oscillatorSettings.addEventListener("detune", handleDetuneChange);
  });
  const init = () => {
    try {
      const reverb = new ReverbNode(context);
      masterGain.connect(reverb).connect(masterPan);
    } catch (err) {
      masterGain.connect(masterPan);
    }
    document.querySelector("modal-dialog").remove();
    context.resume();
    document.addEventListener("contextmenu", (e) => e.preventDefault());
    document.addEventListener("wheel", onWheel);
    document.addEventListener("keydown", onKeyDown);
    document.addEventListener("keyup", onKeyUp);
    const addOscBtn = document.querySelector("#addOscillator");
    addOscBtn.classList.remove("hidden");
    addOscBtn.addEventListener("click", () => {
      const newOscSettingsEl = document.createElement("oscillator-settings");
      const firstOscSettingsEl = document.querySelector("oscillator-settings:first-of-type");
      if (firstOscSettingsEl) {
        firstOscSettingsEl.before(newOscSettingsEl);
      } else {
        addOscBtn.after(newOscSettingsEl);
      }
      oscillatorSettingsEls.push(newOscSettingsEl);
    });
    waveformVisualizer.addEventListener("mousedown", onMouseDown);
    waveformVisualizer.addEventListener("mouseup", onMouseUp);
    waveformVisualizer.addEventListener("touchstart", onTouchStart);
    waveformVisualizer.addEventListener("touchmove", onTouchMove);
    waveformVisualizer.addEventListener("touchend", stopChangedTouchNotes);
    waveformVisualizer.addEventListener("touchcancel", stopChangedTouchNotes);
    document.removeEventListener("keydown", init);
  };
  document.addEventListener("keydown", init);
  document.querySelector("#start").addEventListener("click", init);
};
