/**
 * @param {AudioContext} context
 * @param {number} pan - Between -1 and 1
 * @returns {StereoPannerNode}
 */
export default (context, pan = 0) => {
  if (typeof context.createStereoPanner === "function") {
    return context.createStereoPanner({ pan });
  }

  const panner = context.createPanner();
  panner.panningModel = "equalpower";
  panner.setPosition(pan, 0, 1 - Math.abs(pan));

  return panner;
};
