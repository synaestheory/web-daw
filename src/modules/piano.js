const A0Hz = 27.5;
// [0, 1, ...87] => [27.50, 29.13, ...4186.00]
export const frequencies = [...Array(88).keys()].map((i) => parseFloat((Math.pow(2, i / 12) * A0Hz).toFixed(2)));

const keysNames = [
  ["A"],
  ["A#", "Bb"],
  ["B"],
  ["C"],
  ["C#", "Db"],
  ["D"],
  ["D#", "Eb"],
  ["E"],
  ["F"],
  ["F#", "Gb"],
  ["G"],
  ["G#", "Ab"],
];

// noteNames = [['A0'], ['A#0', 'Bb0'], ['B0'], ...['C7']];
const noteNames = [...Array(8 /* octaves */).keys()].reduce(
  (noteNames, octave) =>
    noteNames.concat(keysNames.map((keyNameArray) => keyNameArray.map((keyName) => keyName + octave))),
  []
);

// notes = [{names: ['A0'], frequency: '27.5'}, ...{names: ['C7'], frequency: '4186.01'}];
const notes = frequencies.map((frequency, i) => ({
  names: noteNames[i],
  frequency,
}));

// Debug
const params = new URLSearchParams(window.location.search);
if (params.get("debug")) {
  console.table(
    notes.reduce((table, { names, frequency }) => {
      table[names[0]] = { frequency, names };
      return table;
    }, {})
  );
}

// full 88 note piano key array
export default notes;
