/**
 * @typedef {Object} Note - General settings for a musical note/tone
 * @prop {number} frequency - frequency of the note
 * @prop {number} [gain=0.75] - gain for the note (between 0 and 1)
 * @prop {OscillatorType} [type] - Type of oscillator/periodic wave
 * @prop {number} [detune] - amount in cents to detune the note (positive/negative)
 */

/**
 * @typedef {OscillatorNode & {gain: AudioParam, id?: string}} ExtendedOscillator
 */

const getOscillator = (context, options) => {
  try {
    return new OscillatorNode(context, options);
  } catch (error) {
    // Safari throws Illegal Constructor -- not yet supported
  }
  const oscillator = context.createOscillator();
  oscillator.type = options.type;
  oscillator.detune.setValueAtTime(options.detune || 0, context.currentTime);
  oscillator.frequency.setValueAtTime(options.frequency, context.currentTime);
  return oscillator;
};

const getGain = (context, options) => {
  try {
    return new GainNode(context, options);
  } catch (error) {
    // Safari throws Illegal Constructor -- not yet supported
  }
  const gainNode = context.createGain();
  if (options.gain) {
    gainNode.gain.setValueAtTime(options.gain, context.currentTime);
  }
  return gainNode;
};

/**
 * @param {AudioContext} context
 * @param {Note} note
 *
 * @returns {ExtendedOscillator}
 */
export default (context, { frequency, gain = 0.75, type, detune }, touchId) => {
  const now = context.currentTime;
  const oscillator = getOscillator(context, { detune, frequency, type });
  oscillator.touchId = touchId;
  const gainNode = getGain(context, { gain });
  oscillator.connect(gainNode);
  oscillator.connect = gainNode.connect.bind(gainNode);

  const fadeLength = 0.03;

  const stopOscillator = oscillator.stop.bind(oscillator);

  oscillator.stop = (when = context.currentTime) => {
    gainNode.gain.setValueAtTime(gainNode.gain.value, when);
    gainNode.gain.exponentialRampToValueAtTime(0.0001, when + fadeLength);
    stopOscillator(when + fadeLength);
  };

  gainNode.gain.setValueAtTime(0.001, context.currentTime);
  gainNode.gain.linearRampToValueAtTime(0.75, context.currentTime + fadeLength);
  oscillator.start(now);

  oscillator.gain = gainNode.gain;
  oscillator.onended = () => {
    oscillator.disconnect();
    gainNode.disconnect();
  };
  return oscillator;
};
