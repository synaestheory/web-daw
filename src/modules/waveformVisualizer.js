/**
 *
 * @param {Map<string, any>} analyserSettings
 * @param {HTMLCanvasElement} canvas
 */
function renderAnalyzerToCanvas(analyserSettings, canvas) {
  const ctx = canvas.getContext("2d");
  const width = canvas.width;
  const height = canvas.height;
  const analyser = analyserSettings.get("analyser");
  const data = new Uint8Array(analyser.frequencyBinCount);
  const sliceWidth = (Math.PI * 2) / (analyser.frequencyBinCount - 1) - 0.05;
  let offset = 0;
  function draw() {
    requestAnimationFrame(draw);
    const backgroundColor = analyserSettings.get("backgroundColor") || "#090909";
    const lineColor = analyserSettings.get("lineColor") || "#9fc";
    analyser.getByteTimeDomainData(data);
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, width, height);
    ctx.lineWidth = 6;
    ctx.strokeStyle = lineColor;
    const size = width > height ? height * 0.5 : width * 0.5;

    [256, 512, 1024, 2048].forEach((ringSize) => {
      ctx.beginPath();
      data.reduce((a, value, i) => {
        const r = (value * size) / ringSize;
        const x = r * Math.cos(a) + width / 2;
        const y = r * Math.sin(a) + height / 2;
        ctx.lineTo(x, y);
        return a + sliceWidth;
      }, offset);
      ctx.stroke();
    });
    offset = offset + sliceWidth;
  }
  draw();
}

export default renderAnalyzerToCanvas;
