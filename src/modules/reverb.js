const reverb_buffers = new Map();

const buffers_to_load = [
  {
    filename: "ir_big_hall.wav",
    key: "big_hall",
  },
  {
    filename: "ir_mini_cave.wav",
    key: "mini_cave",
  },
];

/**
 * @param {AudioContext} context
 */
export function initBuffers(context) {
  buffers_to_load.forEach(({ filename, key }) =>
    fetch(`/modules/reverb/${filename}`)
      .then((res) => res.arrayBuffer())
      .then((ab) => context.decodeAudioData(ab, (audioBuffer) => reverb_buffers.set(key, audioBuffer)))
      .then((audioBuffer) => reverb_buffers.set(key, audioBuffer))
  );
}

export default class ReverbNode extends GainNode {
  constructor(context, options = {}) {
    super(context, options);
    const wetness = Math.max(0, Math.min(1, options.wet ? options.wet : 0.5));
    const dryness = 1 - wetness;
    this._reverb = new ConvolverNode(context, { normalize: false, buffer: reverb_buffers.get("mini_cave") });
    this._wet = new GainNode(context, { gain: wetness });
    this._dry = new GainNode(context, { gain: dryness });
    this._output = new GainNode(context);
    this.connect(this._wet);
    this.connect(this._dry);
    this._wet.connect(this._reverb);
    this._reverb.connect(this._output);
    this._dry.connect(this._output);
    this.connect = this._output.connect.bind(this._output);
  }
  disconnect() {
    GainNode.prototype.disconnect.call(this);
    this._wet.disconnect();
    this._dry.disconnect();
    this._reverb.disconnect();
    this._output.disconnect();
  }
}
