import WaveformSelector from "./WaveformSelector.js";
import "./DetuneSettings.js";

const template = document.createElement("template");
template.innerHTML = `
<style>
  :host {
    display: flex;
    flex-shrink: 0;
    flex: auto;
    width: 100%;
  }
  detune-settings, label {
    display: none;
  }
</style>
<label for="detune">detune</label>
<detune-settings id="detune"></detune-settings>
<waveform-selector id="waveform" value="sine"></waveform-selector>
<button id="trash">Remove</button>
`;

export default class OscillatorSettings extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    /**
     * @type {WaveformSelector}
     */
    this.waveformEl = this.shadowRoot.querySelector("waveform-selector");
    this.detuneEl = this.shadowRoot.querySelector("#detune");
    this.shadowRoot.querySelector('#trash').addEventListener('click', () => this.parentElement.removeChild(this));
  }
  get type() {
    return this.waveformEl.value;
  }
  set type(value) {
    this.waveformEl.value = value;
  }
  get detune() {
    return this.detuneEl.value;
  }
}

(function () {
  const tagName = "oscillator-settings";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, OscillatorSettings);
})();
