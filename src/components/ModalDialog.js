const template = document.createElement("template");
template.innerHTML = `
<style>
  :host::before {
    content: '';
    position: fixed;
    width: 100vw;
    height: 100vh;
    background: hsla(0, 0%, 80%, 0.9);
    pointer-events: none;
    z-index: 0;
  }
  
  :host {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    width: 100vw;
    height: 100vh;
  }
  
  :host.hidden {
    display: none;
  }

  .content {
    background: #444;
    margin: auto;
    max-width: 75%;
    padding: 1rem;
    text-align: center;
    border-radius: 5px;
    z-index: 1;
    box-shadow: 0px 0px 4px #444;
  }

</style>
<div class="content">
  <slot></slot>
</div>
`;

export default class ModalDialog extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
  }
  close() {
    this.classList.add("hidden");
  }
  open() {
    this.classList.remove("hidden");
  }
}

(function () {
  const tagName = "modal-dialog";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, ModalDialog);
})();
