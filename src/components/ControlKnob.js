const template = document.createElement("template");
template.innerHTML = `
<style>
  :host {
    --value: 0;
    --factor: 2.6deg
    display: flex;
  }
  #knob {
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    width: 5rem;
    height: 5rem;
    background: radial-gradient(#ccc, #777, #888);
    transform: rotate(calc(var(--value) * var(--factor)))
  }
  #knob::before {
    position: absolute;
    left: 1rem;
    bottom: 1rem;
    background: radial-gradient(#ccc, #bbb);
    width: 1rem;
    height: 1rem;
    border-radius: 50%;
    content: '';
  }
</style>
<div id="knob"></div>
`;

export default class ControlKnob extends HTMLElement {
  static get observedAttributes() {
    return ["min", "max", "value", "step"];
  }
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    this.knob = this.shadowRoot.querySelector("#knob");
    this._value = 0;
    this._max = 100;
    this._min = 0;
    this._step = 1;
    this.touches = new Map();
    this.mouseDown = this.mouseDown.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.touchStart = this.touchStart.bind(this);
    this.touchMove = this.touchMove.bind(this);
    this.touchEnd = this.touchEnd.bind(this);
  }
  mouseDown() {
    document.addEventListener("mouseup", this.mouseUp);
    document.addEventListener("mousemove", this.mouseMove);
  }
  mouseMove(e) {
    this.value += Math.max(-1, Math.min(1, Math.round(e.movementX))) * this._step;
  }
  mouseUp(e) {
    document.removeEventListener("mouseup", this.mouseUp);
    document.removeEventListener("mousemove", this.mouseMove);
  }
  attributeChangedCallback(name, _, value) {
    this[`_${name}`] = value;
    this.knob.style.setProperty("--factor", `${260 / Math.round((this._max - this._min) / this._step)}deg`);
  }
  connectedCallback() {
    this.knob.addEventListener("mousedown", this.mouseDown);
    this.knob.addEventListener("touchstart", this.touchStart);
  }
  emit(detail) {
    this.dispatchEvent(new CustomEvent("input", { composed: true, bubbled: false, detail }));
  }
  touchStart(e) {
    if (this.touches.size === 0) {
      this.knob.addEventListener("touchmove", this.touchMove, { capture: false, passive: true });
      this.knob.addEventListener("touchend", this.touchEnd, { capture: false, passive: true });
    }
    const touch = e.changedTouches[0];
    this.touches.set(touch.identifier, touch);
  }
  touchMove(e) {
    for (const touch of e.changedTouches) {
      const lastTouch = this.touches.get(touch.identifier);
      if (lastTouch) {
        this.value += Math.max(-1, Math.min(1, Math.round(touch.clientX - lastTouch.clientX))) * this._step;
        this.touches.set(touch.identifier, touch);
      }
    }
  }
  touchEnd(e) {
    const touch = e.changedTouches[0];
    this.touches.delete(touch.identifier);
    if (this.touches.size === 0) {
      this.knob.removeEventListener("touchmove", this.touchMove);
      this.knob.removeEventListener("touchend", this.touchEnd);
    }
  }
  set value(value) {
    this._value = Math.min(this._max, Math.max(this._min, value));
    this.knob.style.setProperty("--value", this._value);
    this.emit(this._value);
  }
  get value() {
    return this._value;
  }
}

(function () {
  const tagName = "control-knob";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, ControlKnob);
})();
