const template = document.createElement("template");
template.innerHTML = `
  <style>
    :host {
      --light-shadow: rgba(255, 255, 255, 0.2);
      --dark-shadow: rgba(0, 0, 0, 0.4);
      width: 100%;
      height: 100%;
      border-top: 3px solid rgba(255, 255, 255, 0.1);
    }
    form {
      display: flex;
      width: 100%;
      height: 100%;
      justify-content: center;
      align-items: center;
    }
    input {
      opacity: 0;
      position: absolute;
    }

    label {
      display: inline-flex;
      margin: 1rem;
    }

    span {
      border-radius: 4px;
      background: var(--light-shadow);
      box-shadow: 
      -2px -2px 4px var(--dark-shadow),
      3px 3px 5px var(--dark-shadow),
      1px 1px 2px var(--dark-shadow) inset,
      -1px -1px 2px var(--dark-shadow) inset;
    }

    svg {
      width: 50px;
      height: 50px;
    }

    input:checked + label span {
      box-shadow:
        -2px -2px 6px var(--light-shadow) inset, 
        5px 5px 6px var(--dark-shadow) inset;
  }

    path {
      fill: none;
      stroke-width: 2px;
      stroke: white;
    }

    input:checked + label path {
      stroke: green;
      filter: url(#glow);
    }

  </style>
  <form>
    <input type="radio" id="sine" name="type" value="sine" checked />
    <label for="sine">
      <span class="icon">
        <svg width="100px" height="100px" viewBox="0 0 100 100">
          <defs>
            <filter id="glow">
              <fegaussianblur class="blur" result="coloredBlur" stddeviation="4"></fegaussianblur>
              <femerge>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="SourceGraphic"></femergenode>
              </femerge>
            </filter>
          </defs>
          <title>sine wave</title>
          <path d="M20,50 C33,20 40,20 50,50 60,80 66,80 80,50"/>
        </svg>
      <span>
    </label>
    <input type="radio" id="sawtooth" name="type" value="sawtooth" />
    <label for="sawtooth">
      <span class="icon">
        <svg width="100px" height="100px" viewBox="0 0 100 100">
          <title>saw wave</title>
          <path d="M25,50 50,75 50,25 75,50"/>
        </svg>
      </span>
    </label>
    <input type="radio" id="square" name="type" value="square" />
    <label for="square">
      <span class="icon">
        <svg width="100px" height="100px" viewBox="0 0 100 100">
          <title>square wave</title>
          <path d="M25,50 25,25 50,25 50,75 75,75 75,50"/>
        </svg>
      <span>
    </label>
    <input type="radio" id="triangle" name="type" value="triangle" />
    <label for="triangle">
      <span class="icon">
        <svg width="100px" height="100px" viewBox="0 0 100 100">
          <title>triangle wave</title>
          <path d="M25,50 37.5,25 62.5,75 75,50"/>
        </svg>
      </span>
    </label>
  </form>
`;

export default class WaveformSelector extends HTMLElement {
  static get observedAttributes() {
    return ["value"];
  }
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    const inputEls = Array.from(this.shadowRoot.querySelectorAll("input[name=type]"));
    this.inputElsByType = new Map(inputEls.map((inputEl) => [inputEl.value, inputEl]));
    this.selectedElement = this.shadowRoot.querySelector(":checked");
    this.handleChange = this.handleChange.bind(this);
  }
  attributeChangedCallback(_attr, _oldValue, newValue) {
    this.select(newValue);
  }
  connectedCallback() {
    this.shadowRoot.addEventListener("change", this.handleChange);
  }
  disconnectedCallback() {
    this.shadowRoot.removeEventListener("change", this.handleChange);
  }
  select(type) {
    if (this.inputElsByType.has(type)) {
      this.selectedElement = this.inputElsByType.get(type);
    }
  }

  next() {
    try {
      this.select(this.selectedElement.nextElementSibling.nextElementSibling.value);
    } catch (err) {
      this.select(this.shadowRoot.querySelector("input:first-of-type").value);
    }
  }

  prev() {
    try {
      this.select(this.selectedElement.previousElementSibling.previousElementSibling.value);
    } catch (err) {
      this.select(this.shadowRoot.querySelector("input:last-of-type").value);
    }
  }

  handleChange(e) {
    this.select(e.target.value);
    this.dispatchEvent(new Event(e.type, e));
  }
  /**
   * @returns {OscillatorType}
   */
  get value() {
    return this.selectedElement.value;
  }

  set value(value) {
    this.select(value);
  }
}

(function () {
  const tagName = "waveform-selector";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, WaveformSelector);
})();
