const template = document.createElement("template");
template.innerHTML = `
<style>
:host {
  display: flex;
  transform: scale(1);
  flex-shrink: 0;
}
/* Styles from https://codepen.io/thebabydino/pen/YPRENB */
input[type='range'] {
  align-self: center;
  margin: -0.75em 0;
  border: solid 1.5em transparent;
  padding: 0;
  width: 13em;
  height: 2.25em;
  outline: none;
  background: repeating-linear-gradient(90deg, #c5b9b9, #c5b9b9 0.0625em, transparent 0.0625em, transparent 2em) no-repeat 50% 0.4375em border-box, repeating-linear-gradient(90deg, #c5b9b9, #c5b9b9 0.0625em, transparent 0.0625em, transparent 2em) no-repeat 50% 4.1875em border-box, linear-gradient(90deg, rgba(195, 183, 183, 0), #c3b7b7, rgba(195, 183, 183, 0)) no-repeat 50% 50% border-box;
  background-size: 12.0625em 0.625em, 12.0625em 0.625em, 100% 2.25em;
  font-size: 1em;
  cursor: pointer;
}
input[type='range'], input[type='range']::-webkit-slider-runnable-track, input[type='range']::-webkit-slider-thumb {
  -webkit-appearance: none;
}
input[type='range']::-webkit-slider-runnable-track {
  position: relative;
  width: 13em;
  height: 0.5em;
  border-radius: .1875em;
  background: rgba(202, 191, 191, 0.825);
}
input[type='range']::-moz-range-track {
  width: 13em;
  height: 0.5em;
  border-radius: .1875em;
  background: rgba(202, 191, 191, 0.825);
}
input[type='range']::-ms-track {
  border: none;
  width: 13em;
  height: 0.5em;
  border-radius: .1875em;
  background: rgba(202, 191, 191, 0.825);
  color: transparent;
}
input[type='range']::-ms-fill-lower {
  display: none;
}
input[type='range']::-webkit-slider-thumb {
  margin-top: -0.75em;
  border: none;
  width: 4em;
  height: 2em;
  border-radius: .5em;
  box-shadow: -.125em 0 .25em #928886,  inset -1px 0 1px #fff;
  background: radial-gradient(#ebe1e0 10%, rgba(235, 225, 224, 0.2) 10%, rgba(235, 225, 224, 0) 72%) no-repeat 50% 50%, radial-gradient(at 100% 50%, #e9dfde, #eae1de 71%, transparent 71%) no-repeat 2.5em 50%, linear-gradient(90deg, #e9dfde, #d0c8c6) no-repeat 100% 50%, radial-gradient(at 0 50%, #d0c6c5, #c6baba 71%, transparent 71%) no-repeat 0.75em 50%, linear-gradient(90deg, #e3d9d8, #d0c6c5) no-repeat 0 50%, linear-gradient(#cdc0c0, #fcf5ef, #fcf5ef, #cdc0c0);
  background-size: 0.825em 100%;
}
input[type='range']::-moz-range-thumb {
  border: none;
  width: 4em;
  height: 2em;
  border-radius: .5em;
  box-shadow: -.125em 0 .25em #928886,  inset -1px 0 1px #fff;
  background: radial-gradient(#ebe1e0 10%, rgba(235, 225, 224, 0.2) 10%, rgba(235, 225, 224, 0) 72%) no-repeat 50% 50%, radial-gradient(at 100% 50%, #e9dfde, #eae1de 71%, transparent 71%) no-repeat 2.5em 50%, linear-gradient(90deg, #e9dfde, #d0c8c6) no-repeat 100% 50%, radial-gradient(at 0 50%, #d0c6c5, #c6baba 71%, transparent 71%) no-repeat 0.75em 50%, linear-gradient(90deg, #e3d9d8, #d0c6c5) no-repeat 0 50%, linear-gradient(#cdc0c0, #fcf5ef, #fcf5ef, #cdc0c0);
  background-size: 0.825em 100%;
}
input[type='range']::-ms-thumb {
  border: none;
  width: 4em;
  height: 2em;
  border-radius: .5em;
  box-shadow: -.125em 0 .25em #928886,  inset -1px 0 1px #fff;
</style>
<input type="range" id="input" min="0" max="100" step="1" value="0"></input>
`;

export default class TrackSlider extends HTMLElement {
  static get observedAttributes() {
    return ["min", "max", "step", "value"];
  }
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    this.input = this.shadowRoot.querySelector("#input");
    this._min = 0;
    this._max = 100;
    this._step = 1;
    this._value = 0;
  }
  connectedCallback() {
    this.input.addEventListener("input", (e) => this.emit(e.target.value));
    this.input.addEventListener("dblclick", (_) => (this.value = 0));
  }
  emit(detail) {
    this.dispatchEvent(new CustomEvent("input", { detail, bubbles: true, composed: true }));
  }
  minmax(value) {
    return Math.min(this.input.getAttribute("max"), Math.max(this.input.getAttribute("min"), value));
  }
  move(amount) {
    this.value = this.value + amount;
  }
  attributeChangedCallback(name, _, value) {
    this.input.setAttribute(name, value);
  }
  get value() {
    return Number(this.input.value);
  }
  set value(newValue) {
    const value = this.minmax(newValue);
    this.input.value = value;
    this._value = value;
    this.emit(value);
  }
}

(function () {
  const tagName = "track-slider";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, TrackSlider);
})();
