const template = document.createElement("template");
template.innerHTML = `
<style>
  :host {
    display: flex;
  }
  :host #root {
    flex-grow: 1;
    border-radius: .15em .15em .25em .25em;
    box-shadow:-.05em 0 0 rgba(255,255,255,0.8) inset, 0 0 .1em #ccc inset, 0 0 .1em rgba(0,0,0,0.2);
    background: var(--background-color, linear-gradient(45deg, #f4f4f4 0%,#d9d9d9 100%));    
  }
  :host #root.active {
    transform: scale(1, -1);
  }
  :host([black]) #root {
    background: var(--background-color, linear-gradient(45deg, #222 0%,#555 100%));
  }
</style>
<div id="root"></div>`;

export default class PianoKey extends HTMLElement {
  static get observedAttributes() {
    return ["frequency"];
  }
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    this.play = this.play.bind(this);
    this.stop = this.stop.bind(this);
    if (!this.getAttribute("frequency")) this.frequency = 440;
  }
  connectedCallback() {
    this.root = this.shadowRoot.querySelector("#root");
  }
  emitEvent(type) {
    const eventSettings = {
      bubbles: false,
      composed: true,
      detail: {
        frequency: this.frequency,
        identifier: this.id,
      },
    };
    const event = new CustomEvent(type, eventSettings);
    this.dispatchEvent(event);
  }
  play() {
    this.root.classList.add("active");
    this.emitEvent("play");
  }
  stop() {
    this.root.classList.remove("active");
    this.emitEvent("stop");
  }
  get frequency() {
    return this.getAttribute("frequency");
  }
  set frequency(frequency) {
    const minFrequency = 20;
    const maxFrequency = 22050;
    const normalizedFrequency = Math.max(minFrequency, Math.min(maxFrequency, frequency));
    return this.setAttribute("frequency", normalizedFrequency);
  }
}

(function () {
  const tagName = "piano-key";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, PianoKey);
})();
