import renderAnalyserToCanvas from "../modules/waveformVisualizer.js";

const template = document.createElement("template");
template.innerHTML = `
<style>
:host {
  height: 100%;
  width: 100%;
  background: #090909;
  display: flex;
  align-items: center;
  justify-content: center;
  overscroll-behavior: none;
}
canvas {
  height: 100%;
}
</style>
<canvas width="1920" height="1920"></canvas>
`;

export default class WaveformVisualizer extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    this.canvas = this.shadowRoot.querySelector("canvas");
  }
  drawAnalyser(analyserSettings) {
    renderAnalyserToCanvas(analyserSettings, this.canvas);
  }
}

(function () {
  const tagName = "waveform-visualizer";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, WaveformVisualizer);
})();
