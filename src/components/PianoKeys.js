import notes from "../modules/piano.js";
import "./PianoKey.js";

const keys = [
  "z",
  "s",
  "x",
  "d",
  "c",
  "v",
  "g",
  "b",
  "h",
  "n",
  "j",
  "m",
  "q",
  "2",
  "w",
  "3",
  "e",
  "r",
  "5",
  "t",
  "6",
  "y",
  "7",
  "u",
  "i",
  "9",
  "o",
  "0",
  "p",
];

const template = document.createElement("template");
template.innerHTML = `
  <style>
    :host {
      display: flex;
      flex-wrap: wrap-reverse;
      flex-direction: row;
      justify-content: flex-start;
      align-items: flex-end;
    }
    piano-key {
      position: relative;
      z-index: 1;
      width: 1.2em;        
      height: 6em;
      flex-shrink: 0;
    }
    piano-key[black] {
      height: 3.5em;
      width: 1em;
      z-index: 2;
      margin-left: -.5em;
      margin-right: -.5em;
    }
  </style>
`;
const keyTemplate = document.createElement("template");
keyTemplate.innerHTML = `<piano-key></piano-key>`;

export default class PianoKeys extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
    shadow.append(
      ...notes.slice(27, 56).map(({ names, frequency }) => {
        const pianoKeyFragment = keyTemplate.content.cloneNode(true);
        const pianoKey = pianoKeyFragment.querySelector("piano-key");
        const name = names[0];
        pianoKey.id = name;
        pianoKey.setAttribute("frequency", frequency);
        if (name.match(/[b#]/)) pianoKey.setAttribute("black", "");
        return pianoKeyFragment;
      })
    );
    const pianoKeys = Array.from(this.shadowRoot.querySelectorAll("piano-key"));
    this.keyboardMap = keys.map((key, i) => ({ key, pianoKey: pianoKeys[i] }));
    this.playing = new Set();
    this.keysPressed = new Set();
    this.eventListeners = new WeakMap();
    this.activeTouches = new Map();
    const eventHandlerKeys = [
      "onKeyDown",
      "onKeyUp",
      "onTouchStart",
      "onTouchMove",
      "onTouchEnd",
      "onMouseOut",
      "onMouseOver",
      "onMouseUp",
      "onMouseDown",
      "onContextMenu",
    ];
    eventHandlerKeys.forEach((eventHandlerKey) => (this[eventHandlerKey] = this[eventHandlerKey].bind(this)));
    const bodyListenerKeys = ["onKeyDown", "onKeyUp"];
    const bodyListeners = bodyListenerKeys.map((eventHandlerKey) => ({
      eventType: eventHandlerKey.toLowerCase().replace("on", ""),
      handler: this[eventHandlerKey],
      element: document.body,
    }));
    const selfListeners = eventHandlerKeys
      .filter((key) => !bodyListenerKeys.includes(key))
      .map((key) => ({ eventType: key.toLowerCase().replace("on", ""), handler: this[key], element: this.shadowRoot }));
    this.eventListeners = bodyListeners.concat(selfListeners);
  }
  connectedCallback() {
    this.eventListeners.map(({ eventType, element, handler }) => element.addEventListener(eventType, handler));
  }
  disconnectedCallback() {
    this.eventListeners.map(({ eventType, element, handler }) => element.removeEventListener(eventType, handler));
  }
  get isPlaying() {
    return this.playing.size > 0 || this.isMouseDown;
  }
  onContextMenu(e) {
    e.preventDefault();
    return false;
  }
  /* Touch Controls */
  onTouchStart(e) {
    e.preventDefault();
    for (const touch of e.changedTouches) {
      this.activeTouches.set(touch.identifier, touch.target);
      this.playKey(touch.target);
    }
  }
  onTouchEnd(e) {
    for (const touch of e.changedTouches) {
      const previouslyTouchedKey = this.activeTouches.get(touch.identifier);
      this.activeTouches.delete(touch.identifier);
      const touchedKeys = new Set(this.activeTouches.values());
      if (!touchedKeys.has(previouslyTouchedKey)) this.stopKey(previouslyTouchedKey);
    }
  }
  onTouchMove(e) {
    e.preventDefault();
    for (const touch of e.changedTouches) {
      const currentTarget = this.shadowRoot.elementFromPoint(touch.clientX, touch.clientY);
      if (!currentTarget) continue;
      const previousTarget = this.activeTouches.get(touch.identifier);
      if (currentTarget === previousTarget) continue;
      this.playKey(currentTarget);
      this.activeTouches.set(touch.identifier, currentTarget);
      const touchedKeys = new Set(this.activeTouches.values());
      if (touchedKeys.has(previousTarget)) continue;
      this.stopKey(previousTarget);
    }
  }
  /* Mouse Controls */
  onMouseOut(e) {
    if (this.isMouseDown && this.isPlaying) this.stopKey(e.target);
  }
  onMouseOver(e) {
    if (this.isMouseDown && this.isPlaying) this.playKey(e.target);
  }
  onMouseDown(e) {
    this.isMouseDown = true;
    this.playKey(e.target);
  }
  onMouseUp(e) {
    this.isMouseDown = false;
    this.stopKey(e.target);
  }
  stopKey(pianoKey) {
    if (!this.playing.has(pianoKey)) return;
    pianoKey.stop();
    this.playing.delete(pianoKey);
  }
  playKey(pianoKey) {
    if (this.playing.has(pianoKey)) return;
    if (!pianoKey.play) return;
    pianoKey.play();
    this.playing.add(pianoKey);
  }
  onKeyDown(e) {
    if (this.keysPressed.has(e.key)) return;
    if (!keys.includes(e.key)) return;
    this.keysPressed.add(e.key);
    const { pianoKey } = this.keyboardMap.find(({ key }) => e.key === key);
    this.playKey(pianoKey);
  }
  onKeyUp(e) {
    if (!keys.includes(e.key)) return;
    this.keysPressed.delete(e.key);
    const { pianoKey } = this.keyboardMap.find(({ key }) => e.key === key);
    this.stopKey(pianoKey);
  }
}

(function () {
  const tagName = "piano-keys";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, PianoKeys);
})();
