import "./TrackSlider.js";

const template = document.createElement("template");
template.innerHTML = `
<style>
  :host {
    display: flex;
  }
</style>
<track-slider min="-1" max="1" step=".0001"></track-slider>
`;

export default class PanSlider extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });
    shadow.appendChild(template.content.cloneNode(true));
  }
}

(function () {
  const tagName = "pan-slider";
  if (window.customElements.get(tagName)) return;
  window.customElements.define(tagName, PanSlider);
})();
